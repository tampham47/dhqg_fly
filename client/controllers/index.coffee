'use strict'

# require order is important
angular.module 'student-fly', [
	'ngAnimate'
	'ngRoute'
	'student-fly.templates'
	'student-fly.home'
	'student-fly.blog'
	'student-fly.user'
]
